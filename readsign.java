import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.UnmodifiableBlockVolume;
import org.spongepowered.api.world.extent.worker.procedure.BlockVolumeVisitor;

@Plugin(id = "readsign", name = "ReadSign", version = "1.0", description = "Reads the text on the sign that the player is looking at")
public class ReadSign {

        @Listener
            public void onCommand(SendCommandEvent event, @First CommandSource source) {
                        if (!(source instanceof Player) || !event.getCommand().equalsIgnoreCase("readsign")) {
                                        return;
                        }

                                Player player = (Player) source;
                                        Location<World> location = player.getLocation();
                                                UnmodifiableBlockVolume blockVolume = location.getExtent().getBlockView(location.getBlockPosition());
                                                        blockVolume.getBlockWorker().iterate(new BlockVolumeVisitor.Breakable() {
                                                                        @Override
                                                                                    public boolean visit(Breakable breakable) {
                                                                                                        if (breakable.getBlockType() == BlockTypes.STANDING_SIGN) {
                                                                                                                                breakable.getExtent().getTileEntity(breakable.getBlockPosition()).ifPresent(tileEntity -> {
                                                                                                                                                            if (tileEntity instanceof org.spongepowered.api.block.tileentity.Sign) {
                                                                                                                                                                                            org.spongepowered.api.block.tileentity.Sign sign = (org.spongepowered.api.block.tileentity.Sign) tileEntity;
                                                                                                                                                                                                                        sign.getSignData().asImmutable().getLines().forEach(line -> {
                                                                                                                                                                                                                                                            player.sendMessage(Text.of(TextColors.GRAY, line.toPlain()));
                                                                                                                                                                                                                        });
                                                                                                                                                            }
                                                                                                                                });
                                                                                                                                                    return false;
                                                                                                        }
                                                                                                                        return true;
                                                                                    }
                                                        });
            }

                public CommandSpec getCommandSpec() {
                            return CommandSpec.builder()
                                            .description(Text.of("Reads the text on the sign that the player is looking at"))
                                                            .permission("readsign.command")
                                                                            .executor(this::onCommand)
                                                                                            .build();
                }

                    public void register() {
                                Sponge.getCommandManager().register(this, getCommandSpec(), "readsign");
                    }
}

                    }
                }
                                                                                                                                                                                                                        })
                                                                                                                                                            }
                                                                                                                                })
                                                                                                        }
                                                                                    }
                                                        })
                        }
            }
}
